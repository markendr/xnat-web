<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ web: mq-context.xml
  ~ XNAT http://www.xnat.org
  ~ Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
  ~ All Rights Reserved
  ~
  ~ Released under the Simplified BSD.
  -->

<!--suppress SpringSecurityFiltersConfiguredInspection -->
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:amq="http://activemq.apache.org/schema/core"
       xmlns:jms="http://www.springframework.org/schema/jms"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
            http://activemq.apache.org/schema/core http://activemq.apache.org/schema/core/activemq-core.xsd
			http://www.springframework.org/schema/jms http://www.springframework.org/schema/jms/spring-jms.xsd">

    <!-- embedded ActiveMQ Broker -->
    <amq:broker id="activeMQBroker" brokerName="activeMQBroker" useJmx="false" persistent="false" schedulerSupport="false" useShutdownHook="true">
        <amq:systemUsage>
            <amq:systemUsage>
                <amq:tempUsage><amq:tempUsage limit="${amq.usage.temp:128mb}"/></amq:tempUsage>
                <amq:memoryUsage><amq:memoryUsage limit="${amq.usage.mem:512mb}"/></amq:memoryUsage>
                <amq:storeUsage><amq:storeUsage limit="${amq.usage.store:1gb}"/></amq:storeUsage>
            </amq:systemUsage>
        </amq:systemUsage>
    </amq:broker>

    <!-- ActiveMQ ConnectionFactory for JMS to use. Spring will find and use the broker we've declared above. -->
    <amq:connectionFactory id="activeMQConnectionFactory" brokerURL="${spring.activemq.broker-url:vm://localhost}" redeliveryPolicyMap="#activeMQRedeliveryPolicyMap"
                           userName="${spring.activemq.user}" password="${spring.activemq.password}" trustAllPackages="true"/>

    <amq:redeliveryPolicyMap id="activeMQRedeliveryPolicyMap">
        <amq:defaultEntry>
            <amq:redeliveryPolicy useExponentialBackOff="true" maximumRedeliveries="4" initialRedeliveryDelay="300000" backOffMultiplier="3" destination="#defaultRequest"/>
        </amq:defaultEntry>
        <amq:redeliveryPolicyEntries>
            <amq:redeliveryPolicy queue="automatedScriptRequest" maximumRedeliveries="0" destination="#automatedScriptRequest"/>
            <amq:redeliveryPolicy queue="moveStoredFileRequest" useExponentialBackOff="true" maximumRedeliveries="4" initialRedeliveryDelay="300000" backOffMultiplier="3" destination="#moveStoredFileRequest"/>
            <amq:redeliveryPolicy queue="prearchiveOperationRequest" useExponentialBackOff="true" maximumRedeliveries="4" initialRedeliveryDelay="300000" backOffMultiplier="3" destination="#prearchiveOperationRequest"/>
            <amq:redeliveryPolicy queue="initializeGroupRequest" useExponentialBackOff="true" maximumRedeliveries="4" initialRedeliveryDelay="300000" backOffMultiplier="3" destination="#initializeGroupRequest"/>
            <amq:redeliveryPolicy queue="processingOperationRequest" useExponentialBackOff="true" maximumRedeliveries="4" initialRedeliveryDelay="300000" backOffMultiplier="3" destination="#processingOperationRequest"/>
            <!-- Additional redelivery policies go here; otherwise default is used. The destination attribute is not apparently used, but is required. -->
        </amq:redeliveryPolicyEntries>
    </amq:redeliveryPolicyMap>

    <!-- lets wrap in a pool to avoid creating a connection per send -->
    <bean name="springConnectionFactory" class="org.springframework.jms.connection.CachingConnectionFactory">
        <property name="targetConnectionFactory" ref="activeMQConnectionFactory" />
    </bean>

    <!-- Spring JMS Template -->
    <bean id="jmsTemplate" class="org.springframework.jms.core.JmsTemplate">
        <property name="connectionFactory" ref="springConnectionFactory" />
    </bean>

    <!-- queues -->
    <amq:queue id="defaultRequest" physicalName="defaultRequest" />
    <amq:queue id="automatedScriptRequest" physicalName="automatedScriptRequest" />
    <amq:queue id="moveStoredFileRequest" physicalName="moveStoredFileRequest" />
    <amq:queue id="prearchiveOperationRequest" physicalName="prearchiveOperationRequest" />
    <amq:queue id="initializeGroupRequest" physicalName="initializeGroupRequest" />
    <amq:queue id="dicomInboxImportRequest" physicalName="dicomInboxImportRequest" />
    <amq:queue id="processingOperationRequest" physicalName="processingOperationRequest" />
    <!-- new request types go here -->

    <!-- listeners for new request types go here -->
    <bean id="dlqMessageListener" class="org.nrg.framework.messaging.DlqListener"/>

    <bean id="errorHandler" class="org.nrg.xnat.services.messaging.XnatMqErrorHandler"/>

    <!-- Spring JMS Listener Container -->
    <jms:listener-container factory-id="jmsListenerContainerFactory" connection-factory="springConnectionFactory" concurrency="10-40" acknowledge="transacted" error-handler="errorHandler">
        <jms:listener destination="ActiveMQ.DLQ" ref="dlqMessageListener" method="onReceiveDeadLetter" />
    </jms:listener-container>

</beans>
